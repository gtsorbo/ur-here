# UR-Here
Interactive University of Rochester guide map application

Using Objective-C and the ArcGIS Online software and APIs.
Some code examples from the ArcGIS API guide were used in development.
Developed as a final project for DMS 101 - Intro to Digital Media Studies (Fall 2014)
