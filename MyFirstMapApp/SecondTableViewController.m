//
//  SecondTableViewController.m
//  UR Here
//
//  Created by Grant Sorbo on 12/10/14.
//  Copyright (c) 2014 DMS101. All rights reserved.
//

#import "SecondTableViewController.h"
#import "ThirdTableViewController.h"

@interface SecondTableViewController ()

@end

@implementation SecondTableViewController {
    NSArray *Residence;
    
    NSArray *Dining;
    NSArray *Recreation;
    NSArray *Libraries;
    NSArray *Academic;
    NSArray *Administrative;
}

-(id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        //Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    //self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
    

    
    Residence = [NSArray arrayWithObjects:@"Residence Quad", @"Hill Court", @"Jackson Court", @"Frat Quad", @"Susan B. Anthony Halls", nil];
    
    Academic = [NSArray arrayWithObjects:@"Rettner Hall",
                @"LeChase Hall",
                @"Lattimore Hall",
                @"Morey Hall",
                @"Dewey Hall",
                @"Bausch & Lomb Hall",
                @"Hoyt Auditorium",
                @"Carol G. Simon",
                @"Schlegel Hall",
                @"Gleason Hall",
                @"Hopeman Hall",
                @"Gavett Hall",
                @"Harkness Hall",
                @"Meliora Hall",
                @"Taylor Hall",
                @"New York State Optics",
                @"Wilmot Hall",
                @"Goergen Hall",
                @"Hylan Hall",
                @"Computer Studies Building",
                @"Hutchinson Hall", nil];
    
    Recreation = [NSArray arrayWithObjects:@"Goergen Athletic Center",
                  @"Todd Union",
                  @"Strong Auditorium",
                  @"Interfaith Chapel",
                  @"Spurrier Gymnasium",
                  @"Sage Art Center",
                  @"GAC Palestra",
                  @"Prince Athletic Complex", nil];
    
    Administrative = [NSArray arrayWithObjects:@"Wallis Hall",
                      @"IT Center",
                      @"UR Tech Store",
                      @"Digital Humanities Center", nil];
    
    Dining = [NSArray arrayWithObjects: @"Wilson Commons",
              @"Frederick Douglass Dining Hall",
              @"Danforth Dining Hall", nil];
    
    Libraries = [NSArray arrayWithObjects:@"Carlson Library",
                 @"Rush Rhees Library",
                 @"Gleason Library",
                 @"Art and Music Library", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    if ([_category isEqualToString:@"Residence"]) {
        return [Residence count];
    }
    else if ([_category isEqualToString:@"Academic"]) {
        return [Academic count];
    }
    else if ([_category isEqualToString:@"Recreation"]) {
        return [Recreation count];
    }
    else if ([_category isEqualToString:@"Administrative"]) {
        return [Administrative count];
    }
    else if ([_category isEqualToString:@"Dining"]) {
        return [Dining count];
    }
    else if ([_category isEqualToString:@"Libraries"]) {
        return [Libraries count];
    }
        
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SecondMenuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if ([_category isEqualToString:@"Residence"]) {
        cell.textLabel.text = [Residence objectAtIndex:indexPath.row];
    }
    else if ([_category isEqualToString:@"Academic"]) {
        cell.textLabel.text = [Academic objectAtIndex:indexPath.row];
    }
    else if ([_category isEqualToString:@"Recreation"]) {
        cell.textLabel.text = [Recreation objectAtIndex:indexPath.row];
    }
    else if ([_category isEqualToString:@"Administrative"]) {
        cell.textLabel.text = [Administrative objectAtIndex:indexPath.row];
    }
    else if ([_category isEqualToString:@"Dining"]) {
        cell.textLabel.text = [Dining objectAtIndex:indexPath.row];
    }
    else if ([_category isEqualToString:@"Libraries"]) {
        cell.textLabel.text = [Libraries objectAtIndex:indexPath.row];
    }
    
    return cell;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
if ([segue.identifier isEqualToString:@"ShowArrayDetail2"]) {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    ThirdTableViewController *destViewController = segue.destinationViewController;
    destViewController.residenceArea = [Residence objectAtIndex:indexPath.row];
    destViewController.title = destViewController.residenceArea;

    }
}
@end
