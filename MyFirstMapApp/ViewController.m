/*
 Copyright 2013 Esri
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) AGSWebMap *webMap;
@property (nonatomic, strong) AGSPopupsContainerViewController *popupVC;

@property (nonatomic, strong) AGSFeatureLayer *featureLayer;
@property (nonatomic, strong) AGSGraphicsLayer *graphicsLayerFromFeatureLayer;

@property (nonatomic, strong) AGSCredential *credentials;
@property (nonatomic) BOOL *locationStatus;

@property (nonatomic, strong) NSMutableArray* popups;


@end

@implementation ViewController
- (IBAction)locationButtonPress:(id)sender {
    if (self.locationStatus) {
        //If location is on when pressed, turn it off
        self.locationStatus = FALSE;
        [self.mapView.locationDisplay stopDataSource];
        [self.locationButton setAlpha:0.5];
    } else if (!self.locationStatus) {
        //if location is off when pressed, turn it on
        self.locationStatus = TRUE;
        [self.mapView.locationDisplay startDataSource];
        [self.locationButton setAlpha:1];
    }
}

-(IBAction)unwindToMainMenu:(UIStoryboardSegue*)sender{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Set Credentials for ArcGIS account
    self.credentials = [[AGSCredential alloc] initWithUser:@"gsorbo_urochester" password:@"mapsandstuff11"];
    self.credentials.authType = AGSAuthenticationTypeToken;
    
    //Initiate Web Map (UR_Here)
    self.webMap = [[AGSWebMap alloc] initWithItemId:@"0d247cf4bb664c879790c1a3903f8733" credential:self.credentials];
    
    //Set delegate for Web Map
    self.webMap.delegate = self;
    
    //Set Location Button Settings
    [self.locationButton setAlpha:0.5];
    self.locationStatus = FALSE;
    [self.mapView.locationDisplay stopDataSource];

    //Set Location Display AutoPan (map moves when location moves)
    self.mapView.locationDisplay.autoPanMode = AGSLocationDisplayAutoPanModeDefault ;
    self.mapView.locationDisplay.wanderExtentFactor = 0.75; //75% of the map's viewable extent
    
    //Set delegate for callout
    self.mapView.callout.delegate = self;
    self.mapView.touchDelegate = self;

    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];

    
    
}

- (void) webMapDidLoad:(AGSWebMap*) webMap {
    //webmap data was retrieved successfully
    
    //Load Web Map into Map View
    [self.webMap openIntoMapView:self.mapView];
    
    //Feature layer
    self.featureLayer = [[AGSFeatureLayer alloc] initWithURL:[NSURL URLWithString:@"http://services3.arcgis.com/Ue34o2qxc9h430hV/arcgis/rest/services/UR_Here_-_Building_Locations_(URL)/FeatureServer"] mode:AGSFeatureLayerModeOnDemand credential:self.credentials];

    
}

- (void) webMap:(AGSWebMap *)webMap didFailToLoadWithError:(NSError *)error {
    //webmap data was not retrieved
    //alert the user
    NSLog(@"Error while loading webmap: %@",[error localizedDescription]);
}

-(void)didOpenWebMap:(AGSWebMap*)webMap intoMapView:(AGSMapView*)mapView{
   	//web map finished opening
    NSLog(@"Web Map Opened into MapView");

}

-(void)webMap:(AGSWebMap*)wm didLoadLayer:(AGSLayer*)layer{
  	 //layer in web map loaded properly
}

-(void)webMap:(AGSWebMap*)wm didFailToLoadLayer:(NSString*)layerTitle url:(NSURL*)url baseLayer:(BOOL)baseLayer federated:(BOOL)federated withError:(NSError*)error{
    NSLog(@"Error while loading layer: %@",[error localizedDescription]);
    
    //you can skip loading this layer
    //[self.webMap continueOpenAndSkipCurrentLayer];
    
    //or you can try loading it with proper credentials if the error was security related
    //[self.webMap continueOpenWithCredential:credential];
}

- (BOOL)prefersStatusBarHidden{
    return NO; //quick win for iOS 7
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AGSMapViewLayerDelegate methods
- (void)mapViewDidLoad:(AGSMapView *) mapView {
    //do something now that the map is loaded
    //for example, show the current location on the map
}

- (void) webMap:(AGSWebMap *)webMap didFetchPopups:(NSArray *)popups forExtent:(AGSEnvelope *)extent {
    //hold on to the results
    for (AGSPopup* popup in popups) {
        //disable editing because this sample does not implement any editing functionality.
        //only permit viewing of popups
        popup.allowEdit = NO;
        popup.allowEditGeometry = NO;
        popup.allowDelete = NO;
        [self.popups addObject:popup];
    }
}

- (void) webMap:(AGSWebMap *)webMap didFinishFetchingPopupsForExtent:(AGSEnvelope *)extent {
    
    //show the popups
    AGSPopupsContainerViewController* pvc =
    [[AGSPopupsContainerViewController alloc]initWithPopups:self.popups];
    pvc.delegate = self;
    [self presentViewController:pvc animated:YES completion:nil];
    
}

#pragma  mark - AGSCalloutDelegte methods
- (void) didClickAccessoryButtonForCallout:(AGSCallout *)callout {
    //fetch popups
    [self.webMap fetchPopupsForExtent:callout.mapLocation.envelope];
    
    //reinitialize the popups array that will hold the results
    self.popups = [[NSMutableArray alloc]init];
}

-(BOOL)callout:(AGSCallout*)callout willShowForFeature:(id<AGSFeature>)feature layer:(AGSLayer<AGSHitTestable>*)layer mapPoint:(AGSPoint*)mapPoint{
    //Specify the callout's contents
    self.mapView.callout.title = (NSString*)[feature attributeForKey:@"TITLE"];
    self.mapView.callout.detail =(NSString*)[feature attributeForKey:@"OBJECTID"];
    return YES;
}


#pragma mark - AGSPopupsContainerDelegate
- (void) popupsContainerDidFinishViewingPopups:(id<AGSPopupsContainer>)popupsContainer{
    [(AGSPopupsContainerViewController*)popupsContainer dismissViewControllerAnimated:YES completion:nil];
}

@end
