//
//  SecondTableViewController.h
//  UR Here
//
//  Created by Grant Sorbo on 12/10/14.
//  Copyright (c) 2014 DMS101. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondTableViewController : UITableViewController

@property (nonatomic, strong) NSString *category;

@end
