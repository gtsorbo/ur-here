//
//  ThirdTableViewController.m
//  UR Here
//
//  Created by Grant Sorbo on 12/10/14.
//  Copyright (c) 2014 DMS101. All rights reserved.
//

#import "ThirdTableViewController.h"

@implementation ThirdTableViewController{
    
    NSArray *ResidenceQuad;
    NSArray *Phase;
    NSArray *Towers;
    NSArray *FratQuad;
}

-(id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        //Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ResidenceQuad = [NSArray arrayWithObjects:@"Gilbert Hall",
                     @"Tiernan Hall",
                     @"Hoeing Hall",
                     @"Lovejoy Hall",
                     @"Crosby Hall",
                     @"Burton Hall", nil];
    
    Phase = [NSArray arrayWithObjects:@"Slater House",
              @"Munro House",
              @"Kendrick House",
              @"Gale House",
              @"Fairchild House",
              @"Chambers House", nil];
    
    Towers = [NSArray arrayWithObjects:@"Anderson Hall",
               @"Wilder Hall",
              @"O'Brien Hall", nil];
    
    FratQuad = [NSArray arrayWithObjects:@"Douglass Leadership House (DLH)",
                @"Sigma Chi (ΣΧ)",
                @"Sigma Alpha Mu (ΣΑΜ)",
                @"Sigma Phi Epsilon (ΣΦΕ)",
                @"Psi Mu (ѱΥ)",
                @"Theta Chi (ΘΧ)",
                @"Alpha Delta Phi (ΑΔΦ)",
                @"Delta Kappa Epsilon (ΔΚΕ)", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    if ([_residenceArea isEqualToString:@"Residence Quad"]) {
        return [ResidenceQuad count];
    }
    else if ([_residenceArea isEqualToString:@"Hill Court"]) {
        return [Phase count];
    }
    else if ([_residenceArea isEqualToString:@"Jackson Court"]) {
        return [Towers count];
    }
    else if ([_residenceArea isEqualToString:@"Frat Quad"]) {
        return [FratQuad count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ThirdMenuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if ([_residenceArea isEqualToString:@"Residence Quad"]) {
        cell.textLabel.text = [ResidenceQuad objectAtIndex:indexPath.row];
    }
    else if ([_residenceArea isEqualToString:@"Hill Court"]) {
        cell.textLabel.text = [Phase objectAtIndex:indexPath.row];
    }
    else if ([_residenceArea isEqualToString:@"Jackson Court"]) {
        cell.textLabel.text = [Towers objectAtIndex:indexPath.row];
    }
    else if ([_residenceArea isEqualToString:@"Frat Quad"]) {
        cell.textLabel.text = [FratQuad objectAtIndex:indexPath.row];
    }

    
    return cell;
}


@end
