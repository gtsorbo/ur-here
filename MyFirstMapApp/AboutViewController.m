//
//  AboutViewController.m
//  UR Here
//
//  Created by Grant Sorbo on 12/10/14.
//  Copyright (c) 2014 DMS101. All rights reserved.
//

#import "AboutViewController.h"

@implementation AboutViewController
@synthesize aboutWebView;

- (void)viewDidLoad {
    NSURL *url = [NSURL URLWithString:@"http://grantsorboroc.wordpress.com/ur-here/"];
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
    
    [aboutWebView loadRequest:requestURL];
    [super viewDidLoad];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
